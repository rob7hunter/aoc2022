(ns d11)

(def div-by #(= (mod %1 %2) 0))

;(def monkey-starting-map-and-fns
;  {0 [[79, 98], #(* % 19), #(if (div-by % 23) 2 3)]
;   1 [[54, 65, 75, 74] #(+ % 6) #(if (div-by % 19) 2 0)]
;   2 [[79, 60, 97] #(* % %) #(if (div-by % 13) 1 3)]
;   3 [[74] #(+ % 3) #(if (div-by % 17) 0 1)]})


(def monkey-starting-map-and-fns
  {0 [[72, 64, 51, 57, 93, 97, 68] #(* % 19) #(if (div-by % 17) 4 7)]
   1 [[62] #(* % 11) #(if (div-by % 3) 3 2)]
   2 [[57, 94, 69, 79, 72] #(+ % 6) #(if (div-by % 19) 0 4)]
   3 [[80, 64, 92, 93, 64, 56] #(+ % 5) #(if (div-by % 7) 2 0)]
   4 [[70, 88, 95, 99, 78, 72, 65, 94] #(+ % 7) #(if (div-by % 2) 7 5)]
   5 [[57, 95, 81, 61] #(* % %) #(if (div-by % 5) 1 6)]
   6 [[79, 99] #(+ % 2) #(if (div-by % 11) 3 1)]
   7 [[68, 98, 62] #(+ % 3) #(if (div-by % 13) 5 6)]})

(def monkey-starting-map (reduce (fn [acc [k v]] (assoc acc k (first v)))
                                 {}
                                 monkey-starting-map-and-fns))

(def activity-map (atom {}))

(defn monkey-do
  [monkey-idx monkey-map op test]
  (let [to-do (get monkey-map monkey-idx)]
    (swap! activity-map (fn [cur] (assoc cur monkey-idx (+ (count to-do)
                                                          (get cur monkey-idx 0)))))
    (reduce (fn [updated-map next-item]
              (let [new-item-level (->> next-item
                                        op
                                        (* 1/3)
                                        (Math/floor)
                                        int)
                    throw-to (test new-item-level)
                    lookup (get updated-map throw-to)]
                (assoc updated-map throw-to (conj lookup new-item-level))))
            (assoc monkey-map monkey-idx [])
            to-do)))

;; Q1

(reduce (fn [mm next-monkey-idx]
          (let [[_ op test] (get monkey-starting-map-and-fns next-monkey-idx)]
            (monkey-do next-monkey-idx mm op test)))
        monkey-starting-map
        (apply concat (repeat 3 (range (count monkey-starting-map)))))

(prn @activity-map)
    
