(ns aoc.util
  (:require
;   [clojure.java.io :as io]
   ))

(defn all-points
  "given a seq of seqs like this: [[a b c] [d e f] ...] where eg, d is at [1,
  0], return all point, where each pt is a [down across] vector. The order is
  left to right, then down"
  [data-rows]
  (if (empty? data-rows)
    '()
    (let [width (-> data-rows first count)]
      (apply concat (map (fn [down] (map (fn [across] [down across])
                                        (range width)))
                         (range (count data-rows)))))))

