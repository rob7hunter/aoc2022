(ns d12
  (:require
   [clojure.java.io :as io]
   [clojure.string :as str]
   [aoc.util :as util]
   ))

(def hm (as-> (slurp "junk.txt") x
          (str/split x #"\n")
          (map vec x)))

(def ap (util/all-points hm))

(prn ap)

(defn h
  [[down across]]
  (nth (nth hm down) across))

(defn pt-up
  [[down across]]
  (if (= down 0)
    nil
    [(- down 1) across]))

(defn pt-down
  [[down across]]
  (if (= down 0)
    nil
    [(- down 1) across]))

(defn pt-left
  [[down across]]
  (if (= down 0)
    nil
    [(- down 1) across]))

(defn pt-right
  [[down across]]
  (if (= down 0)
    nil
    [(- down 1) across]))


(def start (first (filter #(= (h %) \S) ap)))

(defn valid-next-moves
  [partial-path]
  nil)

(defn find-shortest
  []
  (let [
        results (atom [])]
    (loop [partial-path []]
      (if (= (h (last partial-path)) \E)
        (swap! results #(conj % partial-path))
        (doseq [move (valid-next-moves partial-path)]
          (recur (conj partial-path move)))))
    (first (sort-by count results))))
