(ns day3
  (:require
   [clojure.java.io :as io]
   [clojure.set :refer [intersection]]))

;; Q1
(defn priority
  [char]
  (let [is-cap (Character/isUpperCase char)]
    (+ (if is-cap 27 1)
       (- (int char)
          (int (if is-cap \A \a))))))

(defn score-pack-str
  [s]
  (let [compartments (map #(into #{} %) (partition-all (/ (count s) 2) s))]
    (priority (first (apply intersection compartments)))))

(with-open [rdr (io/reader "d3.txt")]
  (prn (reduce (fn [tot pack-str] (+ tot (score-pack-str pack-str)))
               0
               (line-seq rdr))))

;; Q2
(defn score-group
  [group]
  (priority (first (apply intersection (map #(into #{} %) group)))))

(with-open [rdr (io/reader "d3.txt")]
  (prn (reduce (fn [tot group] (+ tot (score-group group)))
               0
               (partition-all 3 (line-seq rdr)))))
