(ns d13
  (:require 
   [clojure.java.io :as io]
   ))

(declare ordered?)

(defn ordered-colls?
  [left right]
  (cond
    (and (empty? left) (empty? right))
    :continue

    (empty? left)
    :ordered

    (empty? right)
    :unordered

    :else
    (let [a (first left)
          b (first right)
          test (ordered? a b)]
      (case test
        :continue (ordered-colls? (rest left) (rest right))
        test))))

(defn ordered?
  [left right]
  (cond
    (and (int? left) (int? right))
    (cond
      (< left right)
      :ordered

      (= left right)
      :continue

      :else
      :unordered)

    (and (coll? left) (coll? right))
    (ordered-colls? left right)

    (coll? left)
    (ordered-colls? left [right])

    :else
    (ordered-colls? [left] right)))

;; Q1
(with-open [rdr (io/reader "d13.txt")]
  (->> (reduce (fn [acc line]
                 (if (= line "")
                   acc
                   (conj acc (eval (read-string line)))))
               []
               (line-seq rdr))
       (partition 2)
       (keep-indexed (fn [idx [left right]]
                       (when (= (ordered? left right) :ordered)
                         (+ 1 idx))))
       (apply +)
       prn))

;; Q2
(def divider-packets [[[2]] [[6]]])
(with-open [rdr (io/reader "d13.txt")]
  (->> (reduce (fn [acc line]
                 (if (= line "")
                   acc
                   (conj acc (eval (read-string line)))))
               divider-packets
               (line-seq rdr))
       (sort (fn [a b]
               (case (ordered? a b)
                 :continue 0
                 :ordered -1
                 :unordered 1)))
       ((fn [result]
          (* (+ 1 (.indexOf result (first divider-packets)))
             (+ 1 (.indexOf result (second divider-packets))))))
       prn))
