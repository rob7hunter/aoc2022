(ns d9
  
(defn convert-to-one-steps
  [moves]
  (->> moves
       (map (fn [move]
              (let [[dir dist-str] (str/split move #" ")
                    dist (Integer/parseInt dist-str)]
                (take dist (repeat dir)))))
       (apply concat)))

(defn move
  ;; one-step move
  [[x y] dir]
  (case dir
    "U" [x (+ y 1)]
    "D" [x (- y 1)]
    "L" [(- x 1) y]
    "R" [(+ x 1) y]))

(defn adjust-tail
 [[hx hy] [tx ty]]
  (cond
    ;; in same col; head is 2-up
    (and (= hx tx) (= (- hy ty) 2))
    [tx (+ 1 ty)]

    ;; in same col; head is 2 down
    (and (= hx tx) (= (- hy ty) -2))
    [tx (- ty 1)]

    ;; in same row; head is 2 left
    (and (= hy ty) (= (- tx hx) 2))
    [(- tx 1) ty]

    ;; in same row; head is 2 right
    (and (= hy ty) (= (- tx hx) -2))
    [(+ tx 1) ty]

    ;; diag case
    (and (not= hx tx) (not= hy ty) (or (>= (abs (- hx tx)) 2)
                                       (>= (abs (- hy ty)) 2)))
    [(+ tx (if (> (- hx tx) 0)
             1
             -1))
     (+ ty (if (> (- hy ty) 0)
             1
             -1))]

    :else
    [tx ty]))

(defn visit
  [so-far new-loc]
  (conj so-far new-loc))

;; Q1
(-> (with-open [rdr (io/reader "d9.txt")]
      (reduce (fn [[head tail visited] move-dir]
                (let [next-head (move head move-dir)
                      next-tail (adjust-tail next-head tail)]
                  [next-head next-tail (visit visited next-tail)]))
              [[0 0] [0 0] #{}]
              (convert-to-one-steps (line-seq rdr))))
    (nth 2)
    count
    prn)


;; Q2

;; a snake is like [[headx heady] ... [tailx taily]]

(defn move-and-adjust-snake
  [snake dir]
  (let [next-head (move (first snake) dir)]
    (loop [to-follow next-head
           to-do (rest snake)
           results [next-head]]
      (if (empty? to-do)
        results
        (let [next-result (adjust-tail to-follow (first to-do))]
          (recur next-result
                 (rest to-do)
                 (conj results next-result)))))))

(-> (with-open [rdr (io/reader "d9.txt")]
      (reduce (fn [[snake visited] move-dir]
                (let [next-snake (move-and-adjust-snake snake move-dir)]
                  [next-snake (visit visited (last next-snake))]))
              [(repeat 10 [0 0]) #{}]
              (convert-to-one-steps (line-seq rdr))))
    (nth 1)
    count
    prn)
