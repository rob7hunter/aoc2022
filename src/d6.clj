(ns day6
  (:require
   [clojure.java.io :as io]
   [clojure.string :as str]
   ))

(defn find-marker
  [s n]
  (->> (range (count s))
       (filter #(apply distinct? (seq (subs s % (min (+ % n) (count s))))))
       first
       (+ n)))

;; Q1
(-> (slurp "d6.txt")
    str/trim
    (find-marker 4))

;; Q2
(-> (slurp "d6.txt")
    str/trim
    (find-marker 14))
