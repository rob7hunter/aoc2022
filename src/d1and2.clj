(ns aoc2022.core
  (:require
   [clojure.java.io :as io]
   ))

;; Day 2, Q2
(defn opp->you
  [opp goal]
  (case goal
    ;; tie:
    \Y opp
    ;; loss:
    \X (case opp
         \A \C
         \B \A
         \C \B)
    ;; win:
    \Z (case opp
         \A \B
         \B \C
         \C \A)))

(with-open [rdr (io/reader "resources/d2.txt")]
  (reduce (fn [tot-score next-line]
            (let [opp (get next-line 0)
                  goal (get next-line 2)
                  you (opp->you opp goal)]
              (+ (case you
                   \A 1
                   \B 2
                   \C 3)
                 (case goal
                   \X 0
                   \Y 3
                   \Z 6)
                 tot-score)))
          0
          (line-seq rdr)))

;; Day 2, Q1
(defn rps<
  [opp you]
  (case opp
    \A (= you \Y)
    \B (= you \Z)
    \C (= you \X)))

(defn rps=
  [opp you]
  (case opp
    \A (= you \X)
    \B (= you \Y)
    \C (= you \Z)))

(with-open [rdr (io/reader "resources/d2.txt")]
  (reduce (fn [tot-score next-line]
            (let [opp (get next-line 0)
                  you (get next-line 2)]
              (+ (case you
                   \X 1
                   \Y 2
                   \Z 3)
                 (cond
                   (rps< opp you) 6
                   (rps= opp you) 3
                   :else 0)
                 tot-score)))
          0
          (line-seq rdr)))

(comment
;; Day 1, Q1
(with-open [rdr (io/reader "resources/q1.txt")]
  (reduce (fn [[cur-sum best-so-far] next-line]
            (if (zero? (count next-line))
              [0 (max cur-sum best-so-far)]
              [(+ (Integer/parseInt next-line) cur-sum)
               best-so-far]))
          [0 0]
          (line-seq rdr)))
;; if you come back to this, remember you have to look at both the cur-sum and best-so-far to see which is bigger


;; Day 1, Q2 -- let's just reduce and sort
(with-open [rdr (io/reader "resources/q1.txt")]
  (let [[c s]
        (reduce (fn [[cur-sum s] next-line]
                  (if (zero? (count next-line))
                    [0 (conj s (* -1 cur-sum))]
                    [(+ (Integer/parseInt next-line) cur-sum) s]))
                [0 []]
                (line-seq rdr))]
    (apply + (take 3 (sort (conj s c))))))

)
