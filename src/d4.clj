(ns day4
  (:require
   [clojure.java.io :as io]
   [clojure.set :refer [intersection]]
   [clojure.string :as str]))

;; Q1

(defn range-contained-in?
  [r1 r2]
  (and (<= (first r1) (first r2))
       (>= (second r1) (second r2))))

(defn line->ranges
  [line]
  (map (fn [range-str]
         (map #(Integer/parseInt %) (str/split range-str #"-")))
       (str/split line #",")))

(defn count-em
  [overlap-fn]
  (with-open [rdr (io/reader "d4.txt")]
    (->> (line-seq rdr)
         (map line->ranges)
         (filter #(or (apply overlap-fn %)
                      (apply overlap-fn (reverse %))))
         count)))

(prn (count-em range-contained-in?))

;; Q2

(defn overlap<=?
  [r1 r2]
  (and (<= (first r1) (second r2))
       (>= (second r1) (first r2))))

(prn (count-em overlap<=?))
