(ns d10
  (:require
   [clojure.java.io :as io]
   [clojure.string :as str]
   ))

;; returns [cycles used up, change to X register]
(defn raw-op->changes
  [raw-op]
  (if (= raw-op "noop")
    [1 0]
    (let [amt (-> raw-op
                  (str/split #" ")
                  (nth 1)
                  (Integer/parseInt))]
      [2 amt])))

(def cycle-data
  (with-open [rdr (io/reader "d10.txt")]
    (loop [changes (->> (line-seq rdr)
                        (map raw-op->changes))
           ticks 0
           x 1
           results []]
      (if (empty? changes)
        results
        (let [next (first changes)
              next-tick (+ (first next) ticks)
              next-x (+ (second next) x)]
          (recur (rest changes)
                 next-tick
                 next-x
                 (conj results [next-tick next-x])))))))

;; Q1
(defn cycle-strength
  [cycle]
  (let [one-past-i (first (keep-indexed (fn [idx [tick _]]
                                          (when (>= tick cycle)
                                            idx))
                                        cycle-data))]
    (-> cycle-data
        (nth (- one-past-i 1))
        second
        (* cycle))))

(def q1 (apply + (map cycle-strength (map #(+ 20 %) (map #(* 40 %) (range 6))))))

;; Q2
;(prn cycle-data)

(loop [i 0
       x 1
       sprite-to-go cycle-data]
  ;; at pos i, we are starting cycle (i+1)
  (when (seq sprite-to-go)
    (let [[upcoming-cycle upcoming-x] (first sprite-to-go)
          next-pos (+ 1 i)]
      (print (if (<= (- x 1) (mod i 40) (+ x 1))
               "#"
               "."))
      (when (zero? (mod next-pos 40))
        ;(prn [i x sprite-to-go])
        (print "\n"))
      (recur next-pos
             (if (= next-pos upcoming-cycle)
               upcoming-x
               x)
             (if (= next-pos upcoming-cycle)
               (rest sprite-to-go)
               sprite-to-go)))))
