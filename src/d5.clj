(ns d5
  (:require
   [clojure.java.io :as io]
   [clojure.string :as str]
   ))

(def stacks
  (atom [["D" "L" "V" "T" "M" "H" "F"]
         ["H" "Q" "G" "J" "C" "T" "N" "P"]
         ["R" "S" "D" "M" "P" "H"]
         ["L" "B" "V" "F"]
         ["N" "H" "G" "L" "Q"]
         ["W" "B" "D" "G" "R" "M" "P"]
         ["G" "M" "N" "R" "C" "H" "L" "Q"]
         ["C" "L" "W"]
         ["R" "D" "L" "Q" "J" "Z" "M" "T"]]))

(defn move
  [num from-1idx to-1idx]
  (let [from-idx (- from-1idx 1)
        to-idx (- to-1idx 1)
        from-crane (nth @stacks from-idx)
        [from-to-keep from-to-move] (split-at (- (count from-crane) num) from-crane)
        to-crane (nth @stacks to-idx)]
    (swap! stacks
           (fn [s]
             (-> s
                 (assoc from-idx from-to-keep)
                 (assoc to-idx (concat to-crane
                                       from-to-move)))))))
                                       ;; Q1(reverse from-to-move))))))))

(with-open [rdr (io/reader "d5.txt")]
  (doseq [op (line-seq rdr)]
    (eval (read-string op))))

(->> @stacks
     (map last)
     (str/join "")
     prn)

