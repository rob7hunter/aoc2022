(ns day8
  (:require
   [clojure.java.io :as io]
   [clojure.string :as str]
   ))

(def forest
  (with-open [rdr (io/reader "d8.txt")]
    (mapv #(as-> % row (str/split row #"") (mapv read-string row))
          (line-seq rdr))))

(def height (count forest))
(def width (count (first forest)))

(defn 🎄
  [down across]
  (nth (nth forest down) across))

(defn heights-up-from
  [down across]
  (reverse (map #(🎄 % across) (range 0 down))))

(defn heights-down-from
  [down across]
  (map #(🎄 % across) (range (+ 1 down) height)))

(defn heights-to-left
  [down across]
  (reverse (map #(🎄 down %) (range 0 across))))

(defn heights-to-right
  [down across]
  (map #(🎄 down %) (range (+ 1 across) width)))

(defn border?
  [down across]
  (or (= down 0)
      (= across 0)
      (= down (- 1 height))
      (= across (- 1 width))))

(defn is-visible?
  [down across]
  (or (border? down across)
      (let [lower? #(< % (🎄 down across))]
        (or (every? lower? (heights-up-from down across))
            (every? lower? (heights-down-from down across))
            (every? lower? (heights-to-left down across))
            (every? lower? (heights-to-right down across))))))

(defn count-row-visible
  [down]
  (reduce (fn [row-tot across]
            (if (is-visible? down across)
              (+ 1 row-tot)
              row-tot))
          0
          (range 0 width)))

;; Q1
(prn (apply + (map count-row-visible (range 0 height))))

;; Q2

(defn count-trees-you-can-see
  [trees-ahead your-height]
  (let [[tinies the-rest] (split-with #(< % your-height) trees-ahead)]
    (+ (count tinies)
       (if (empty? the-rest)
         0
         1))))

(defn scenic-score
  [down across]
  (let [h (🎄 down across)
        viewing-dist #(count-trees-you-can-see % h)]
    (* (viewing-dist (heights-up-from down across))
       (viewing-dist (heights-down-from down across))
       (viewing-dist (heights-to-left down across))
       (viewing-dist (heights-to-right down across)))))

(def q2
  (apply max (map (fn [down]
                    (apply max (map #(scenic-score down %) (range 0 width))))
                  (range 0 height))))
