(ns day7
  (:require
   [clojure.java.io :as io]
   [clojure.string :as str]
   ))

(defn parse-dir-name
  [dir-output]
  (apply str (drop 4 dir-output)))

(defn parse-file-size
  [file-output]
  (Integer/parseInt (first (str/split file-output #" "))))

;; just one level
(defn discover-dir-immediate
  [log name]
  (assert (= (first log)
             (format "$ cd %s" name)))
  (assert (= (second log) "$ ls"))
  (let [[in-this-dir the-rest]
        (->> (drop 2 log)
             (split-with #(not= (first %) \$)))
        {:keys [dirs files]} (group-by #(if (str/starts-with? % "dir ") :dirs :files)
                                       in-this-dir)
        immediate-size (apply + (map parse-file-size files))]
    {:size immediate-size
     :dirs (map parse-dir-name dirs)
     :rest-log the-rest}))

;; when you cd into you need a corresponding out, except the first time
;; so push

(defn deep-discover
  [log name]
  (let [{:keys size dirs rest-log} (discover-dir-immediate log name)]
    (loop [num-pushes 0
           log rest-log
           dirs-to-go dirs
           dir-results []]
      (if (empty? dirs-to-go)
        (do (prn (format "should all be 'cd ..': %s" (take num-pushes log)))
            {:keys size :dirs dir-results :log (take num-pushes log)})
        




(defn discover-dir
  [log name parent-dirs]
  

(with-open [rdr (io/reader "junk.txt")]
  (prn (discover-dir (line-seq rdr) "/" 0)))
