from pprint import pprint

root = None
cur = None

with open("d7.txt") as file:
    for line in file:
        line = line.strip()
        if line.startswith("$ cd .."):
            cur = cur["parent"]
        elif line.startswith("$ cd "):
            fresh_dir = {}
            if cur is not None:
                cur["dirs"].append(fresh_dir)
            fresh_dir["parent"] = cur
            fresh_dir["name"] = line[5:]
            fresh_dir["dirs"] = []
            fresh_dir["files"] = []
            cur = fresh_dir
            if root is None:
                root = cur
        elif line.startswith("$ ls"):
            pass
        elif not line.startswith("$"):
            if line.startswith("dir"):
                pass
            else:
                # it's a file - append the size
                file_size = int(line.split(" ")[0])
                cur["files"].append(file_size)

def totalSizeOfNode(node):
    return sum(node["files"]) + sum(map(totalSizeOfNode, node["dirs"]))

# Q1 - total sizes of dirs <= 100000 in size
def Q1():
    q1_tot = 0
    def foo(node):
        nonlocal q1_tot
        s = totalSizeOfNode(node)
        if s <= 100000:
            q1_tot += s
        for sub in node["dirs"]:
            foo(sub)
    foo(root)
    return q1_tot

print(Q1())

# Q2

def Q2():
    deleteable_amts = []
    total_used = totalSizeOfNode(root)
    MAX_STORAGE = 70000000
    NEEDED_FREE_SPACE = 30000000
    cur_free_space = MAX_STORAGE - total_used
    def foo(node):
        nonlocal deleteable_amts
        s = totalSizeOfNode(node)
        if s + cur_free_space >= NEEDED_FREE_SPACE:
            deleteable_amts.append(s)
        for sub in node["dirs"]:
            foo(sub)
    foo(root)
    deleteable_amts.sort()
    return deleteable_amts[0]

print(Q2())
